package itis.socialtest;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;
import itis.socialtest.AnalyticsService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AnalyticsServiceImpl implements AnalyticsService {
    @Override
    public List<Post> findPostsByDate(List<Post> posts, String date) {
        return posts.stream().filter(post -> post.getDate().equals(date)).collect(Collectors.toList());
    }

    @Override
    public Boolean checkPostsThatContainsSearchString(List<Post> posts, String searchString) {
        return posts.stream().anyMatch(post -> post.getContent().contains(searchString));
    }

    @Override
    public List<Post> findAllPostsByAuthorNickname(List<Post> posts, String nick) {
        return posts.stream()
                .filter(post -> post.getAuthor().getNickname().equals(nick))
                .collect(Collectors.toList());
    }

    @Override
    public String findMostPopularAuthorNickname(List<Post> posts) {
        Map<String, Long> result = posts.stream()
                .collect(Collectors.toMap(post -> post.getAuthor().getNickname(), p -> posts.stream()
                        .filter(q -> q.getAuthor().equals(p.getAuthor()))
                        .mapToLong(Post::getLikesCount)
                        .sum()));

        ArrayList<String> keySets = new ArrayList<>();
        keySets.addAll(result.keySet());
        Collections.sort(keySets);
        return keySets.get(0);
    }
}

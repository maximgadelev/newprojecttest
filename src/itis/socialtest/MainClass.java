package itis.socialtest;

import itis.socialtest.entities.Post;
import itis.socialtest.entities.*;

import java.io.*;
import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("C:\\Users\\111\\socialnetworktest\\src\\itis\\socialtest\\resources\\PostDatabase.csv", "C:\\Users\\111\\socialnetworktest\\src\\itis\\socialtest\\resources\\Authors.csv");

    }


    private Author getAuthorById(ArrayList<Author> authors, Long id) {
        return authors.stream().filter(author -> author.getId().equals(id)).findFirst().orElse(null);
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        try {
            File fileOne = new File(postsSourcePath);
            File fileTwo = new File(authorsSourcePath);
            FileReader fileReaderOne = new FileReader(fileOne);
            FileReader fileReaderTwo = new FileReader(fileTwo);
            BufferedReader bufferedReaderOne = new BufferedReader(fileReaderOne);
            BufferedReader bufferedReaderTwo = new BufferedReader(fileReaderTwo);
            ArrayList<Author> authorArrayList = new ArrayList<>();
            ArrayList<Post> postArrayList = new ArrayList<>();
            String post;
            //поля автора
            long authorId;
            String author;
            String birthdayDate;
            String nickName;
            //парсинг авторов
            while (true) {
                author = bufferedReaderTwo.readLine();
                if (author == null) {
                    break;
                }
                String[] strings = author.split(",");
                authorId = Long.parseLong(strings[0]);
                nickName = strings[1];
                birthdayDate = strings[2];
                authorArrayList.add(new Author(authorId, nickName, birthdayDate));
            }
            bufferedReaderTwo.close();
            //парсинг поста
            while (true) {
                post = bufferedReaderOne.readLine();
                if (post == null) {
                    break;
                }
                String[] strings = post.split(", ");
                for (int i = 3; i < strings.length; i++) {
                    strings[3] += ", " + strings[i];
                }
                postArrayList.add(new Post(strings[2], strings[3], Long.parseLong(strings[1]),
                        getAuthorById(authorArrayList, Long.parseLong(strings[0]))));
            }


            bufferedReaderOne.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
